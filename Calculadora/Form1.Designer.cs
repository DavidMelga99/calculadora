﻿namespace Calculadora
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnAC = new System.Windows.Forms.Button();
            this.btninvertir = new System.Windows.Forms.Button();
            this.btndiv = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btnmulti = new System.Windows.Forms.Button();
            this.btnresta = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn7 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn0 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btnsuma = new System.Windows.Forms.Button();
            this.btnigual = new System.Windows.Forms.Button();
            this.numTxt = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnAC
            // 
            this.btnAC.Location = new System.Drawing.Point(60, 63);
            this.btnAC.Name = "btnAC";
            this.btnAC.Size = new System.Drawing.Size(113, 39);
            this.btnAC.TabIndex = 1;
            this.btnAC.Text = "AC";
            this.btnAC.UseVisualStyleBackColor = true;
            this.btnAC.Click += new System.EventHandler(this.button1_Click);
            // 
            // btninvertir
            // 
            this.btninvertir.Location = new System.Drawing.Point(206, 63);
            this.btninvertir.Name = "btninvertir";
            this.btninvertir.Size = new System.Drawing.Size(39, 39);
            this.btninvertir.TabIndex = 2;
            this.btninvertir.Text = "+/-";
            this.btninvertir.UseVisualStyleBackColor = true;
            // 
            // btndiv
            // 
            this.btndiv.Location = new System.Drawing.Point(272, 63);
            this.btndiv.Name = "btndiv";
            this.btndiv.Size = new System.Drawing.Size(39, 39);
            this.btndiv.TabIndex = 3;
            this.btndiv.Text = "/";
            this.btndiv.UseVisualStyleBackColor = true;
            // 
            // btn9
            // 
            this.btn9.Location = new System.Drawing.Point(206, 118);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(39, 39);
            this.btn9.TabIndex = 4;
            this.btn9.Text = "9";
            this.btn9.UseVisualStyleBackColor = true;
            this.btn9.Click += new System.EventHandler(this.button3_Click);
            // 
            // btn6
            // 
            this.btn6.Location = new System.Drawing.Point(206, 172);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(39, 39);
            this.btn6.TabIndex = 5;
            this.btn6.Text = "6";
            this.btn6.UseVisualStyleBackColor = true;
            // 
            // btnmulti
            // 
            this.btnmulti.Location = new System.Drawing.Point(272, 118);
            this.btnmulti.Name = "btnmulti";
            this.btnmulti.Size = new System.Drawing.Size(39, 39);
            this.btnmulti.TabIndex = 6;
            this.btnmulti.Text = "X";
            this.btnmulti.UseVisualStyleBackColor = true;
            // 
            // btnresta
            // 
            this.btnresta.Location = new System.Drawing.Point(272, 172);
            this.btnresta.Name = "btnresta";
            this.btnresta.Size = new System.Drawing.Size(39, 39);
            this.btnresta.TabIndex = 7;
            this.btnresta.Text = "-";
            this.btnresta.UseVisualStyleBackColor = true;
            // 
            // btn8
            // 
            this.btn8.Location = new System.Drawing.Point(134, 118);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(39, 39);
            this.btn8.TabIndex = 8;
            this.btn8.Text = "8";
            this.btn8.UseVisualStyleBackColor = true;
            // 
            // btn7
            // 
            this.btn7.Location = new System.Drawing.Point(60, 118);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(39, 39);
            this.btn7.TabIndex = 9;
            this.btn7.Text = "7";
            this.btn7.UseVisualStyleBackColor = true;
            // 
            // btn4
            // 
            this.btn4.Location = new System.Drawing.Point(60, 172);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(39, 39);
            this.btn4.TabIndex = 10;
            this.btn4.Text = "4";
            this.btn4.UseVisualStyleBackColor = true;
            // 
            // btn5
            // 
            this.btn5.Location = new System.Drawing.Point(134, 172);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(39, 39);
            this.btn5.TabIndex = 11;
            this.btn5.Text = "5";
            this.btn5.UseVisualStyleBackColor = true;
            // 
            // btn1
            // 
            this.btn1.Location = new System.Drawing.Point(60, 230);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(39, 39);
            this.btn1.TabIndex = 12;
            this.btn1.Text = "1";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn2
            // 
            this.btn2.Location = new System.Drawing.Point(134, 230);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(39, 39);
            this.btn2.TabIndex = 13;
            this.btn2.Text = "2";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // btn0
            // 
            this.btn0.Location = new System.Drawing.Point(60, 287);
            this.btn0.Name = "btn0";
            this.btn0.Size = new System.Drawing.Size(185, 39);
            this.btn0.TabIndex = 14;
            this.btn0.Text = "0";
            this.btn0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn0.UseVisualStyleBackColor = true;
            // 
            // btn3
            // 
            this.btn3.Location = new System.Drawing.Point(206, 230);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(39, 39);
            this.btn3.TabIndex = 15;
            this.btn3.Text = "3";
            this.btn3.UseVisualStyleBackColor = true;
            // 
            // btnsuma
            // 
            this.btnsuma.Location = new System.Drawing.Point(272, 230);
            this.btnsuma.Name = "btnsuma";
            this.btnsuma.Size = new System.Drawing.Size(39, 39);
            this.btnsuma.TabIndex = 16;
            this.btnsuma.Text = "+";
            this.btnsuma.UseVisualStyleBackColor = true;
            // 
            // btnigual
            // 
            this.btnigual.Location = new System.Drawing.Point(272, 287);
            this.btnigual.Name = "btnigual";
            this.btnigual.Size = new System.Drawing.Size(39, 39);
            this.btnigual.TabIndex = 17;
            this.btnigual.Text = "=";
            this.btnigual.UseVisualStyleBackColor = true;
            // 
            // numTxt
            // 
            this.numTxt.Location = new System.Drawing.Point(60, 22);
            this.numTxt.Name = "numTxt";
            this.numTxt.Size = new System.Drawing.Size(251, 20);
            this.numTxt.TabIndex = 18;
            this.numTxt.Text = "0";
            this.numTxt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 348);
            this.Controls.Add(this.numTxt);
            this.Controls.Add(this.btnigual);
            this.Controls.Add(this.btnsuma);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btn0);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.btn5);
            this.Controls.Add(this.btn4);
            this.Controls.Add(this.btn7);
            this.Controls.Add(this.btn8);
            this.Controls.Add(this.btnresta);
            this.Controls.Add(this.btnmulti);
            this.Controls.Add(this.btn6);
            this.Controls.Add(this.btn9);
            this.Controls.Add(this.btndiv);
            this.Controls.Add(this.btninvertir);
            this.Controls.Add(this.btnAC);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Calculadora";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnAC;
        private System.Windows.Forms.Button btninvertir;
        private System.Windows.Forms.Button btndiv;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btnmulti;
        private System.Windows.Forms.Button btnresta;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn0;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btnsuma;
        private System.Windows.Forms.Button btnigual;
        private System.Windows.Forms.TextBox numTxt;
    }
}

